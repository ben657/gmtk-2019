﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperSecret : MonoBehaviour
{
    private float timer = 0;
    public GameObject BagFairy;
    // Start is called before the first frame update
    void OnTriggerEnter2D()
    {
        print("ACTIVE");
        timer = 0.25f;
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > 0)
        {
            if(BagFairy != null)
            {
                BagFairy.SetActive(true);
                timer -= Time.deltaTime;
            }
            
        }
        else if(BagFairy != null)
        {
            BagFairy.SetActive(false);
        }
    }
}
