﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class GameManager
{
    public static int levelsOffset = 1;
    public static bool isSinglePlayer = true;

    static int currentLevel = -1;

    public static int p1Score = 0;
    public static int p2Score = 0;

    public static void LoadLevel(int level)
    {
        currentLevel = level;
        PlayerPrefs.SetInt("LastLevel", currentLevel);
        SceneManager.LoadScene(levelsOffset + level, LoadSceneMode.Single);
    }

    public static void LoadNextLevel()
    {
        LoadLevel(currentLevel + 1);
    }

    public static void Reload()
    {
        LoadLevel(currentLevel);
    }

    public static void SetCurrentLevel(int buildIndex)
    {
        currentLevel = buildIndex - levelsOffset;
    }
}
