﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class AICharacterController : MonoBehaviour
{
    CharacterController character;

    float elapsedTime = 0.0f;
    float fireTime = 0.0f;
    float aimTime = 0.0f;
    float fireAngle = 0.0f;

    bool aimed = false;
    bool fired = false;

    private void Awake()
    {
        character = GetComponent<CharacterController>();
    }

    private void Start()
    {
        LevelManager manager = FindObjectOfType<LevelManager>();
        fireTime = manager.aiData.time;
        fireAngle = manager.aiData.angle;
        aimTime = fireTime - 2.0f;
    }

    private void Update()
    {
        elapsedTime += Time.deltaTime;

        if(elapsedTime > aimTime && !aimed)
        {
            aimed = true;
            
            float angle = Mathf.Deg2Rad * fireAngle;
            Vector2 direction = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
            character.Aim(direction);
        } 
        if(elapsedTime > fireTime && !fired)
        {
            fired = true;
            character.Shoot();
        }
    }
}
