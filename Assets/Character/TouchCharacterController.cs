﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class TouchCharacterController : MonoBehaviour
{
    CharacterController character;

    bool lastTouching = false;
    bool touching = false;
    Vector2 origin;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 currentPos = Vector2.zero;
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) touching = true;
            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) touching = false;
            currentPos = touch.position;
        }
        if(Input.GetMouseButtonDown(0))
        {
            touching = true;
            origin = Input.mousePosition;
        }
        if(Input.GetMouseButton(0))
        {
            currentPos = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            touching = false;
        }

        if(touching)
        {
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 direction = ((Vector2)pos - currentPos).normalized;
            print(direction);
            character.Aim(direction);
        }
        if(lastTouching && !touching)
        {
            character.Shoot();
            touching = false;
        }

        lastTouching = touching;
    }
}
