﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public LayerMask characterLayer;

    public bool debug = false;

    public Bullet bulletPrefab;

    public float aimSpeed = 1.0f;
    public Transform bulletSpawn;
    public SpriteRenderer sprite;
    public Transform marker;
    public bool showMarker = false;
    public float maxMarkerDist = Mathf.Infinity;

    public AudioClip FireSound, DeathSound;

    public GameObject Explosion;

    Vector3 targetAim = Vector3.up;
    bool hasFired = false;

    private void Start()
    {
        targetAim = transform.up;
    }

    public bool HasFired()
    {
        return hasFired;
    }

    public void Shoot()
    {
        if (hasFired && !debug) return;

        SoundManager.instance.PlaySingle(FireSound);

        hasFired = true;
        Bullet bullet = Instantiate(bulletPrefab);
        bullet.transform.position = bulletSpawn.position;

        bullet.SetDirection(transform.up);
        bullet.SetOwner(this);

        if (!sprite) return;
        TrailRenderer trail = bullet.GetComponentInChildren<TrailRenderer>();
        trail.startColor = Color.white;
        Color end = sprite.color;
        end.a = 0.0f;
        trail.endColor = end;
    }

    public void Aim(Vector3 direction)
    {
        targetAim = direction.normalized;
    }

    public void Aim(float angle)
    {
        float rad = angle * Mathf.Deg2Rad;
        Vector2 direction = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));
        Aim(direction);
    }

    public void StopAim()
    {
        targetAim = transform.up;
    }

    void UpdatePreviewLine()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Bullet bullet = collision.gameObject.GetComponent<Bullet>();
        if (bullet == null) return;

        Die();
        bullet.Die();
    }

    private void Update()
    {
        float angle = Vector3.SignedAngle(targetAim, Vector3.up, Vector3.forward);
        Quaternion q = Quaternion.AngleAxis(angle, -Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * aimSpeed);

        if (!marker) return;
        marker.gameObject.SetActive(false);
        if (showMarker)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, maxMarkerDist, characterLayer);
            if (hit.collider)
            {
                Vector3 pos = hit.point;
                marker.gameObject.SetActive(true);
                marker.position = pos;
                marker.up = -hit.normal;
            }
        }
    }

    IEnumerator PlayDeathSound()
    {
        SoundManager.instance.PlaySingle(DeathSound);
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }

    public void Die()
    {
        foreach(SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>())
        {
            Destroy(sr);
        }
        Explosion.GetComponent<ParticleSystem>().Play();

        StartCoroutine(PlayDeathSound());
    }
}
