﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

[RequireComponent(typeof(CharacterController))]
public class GamepadCharacterController : MonoBehaviour
{
    CharacterController character;
    private Player player; // The Rewired Player
    public int playerId = 0;

    float prevFire = 0.0f;

    private void Awake()
    {
        character = GetComponent<CharacterController>();
    }

    private void Start()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        player = ReInput.players.GetPlayer(playerId);
    }

    private void Update()
    {
        float x = player.GetAxis("Horizontal");
        float y = player.GetAxis("Vertical");
        if (x == 0 && y == 0)
        {
            character.StopAim();
        }
        else
        {
            Vector2 direction = new Vector2(x, y);
            direction.Normalize();

            character.Aim(direction);
        }

        if (player.GetAxis("Fire") > 0.0f && prevFire == 0.0f)
        {
            character.Shoot();
        }

        prevFire = player.GetAxis("Fire");

        if(player.GetButtonDown("Reset"))
        {
            GameManager.Reload();
        }
    }
}
