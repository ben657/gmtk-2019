﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DramaticSlowdown : MonoBehaviour
{
    public LayerMask characterMask;

    public float castRadius = 0.5f;

    public float maxDist = 3.0f;
    public float minDist = 1.0f;

    public float timeMultiplier = 0.5f;
    public float maxZoom = 1.0f;
    public float maxCameraPull = 1.0f;

    float amount = 0.0f;

    float baseTimeScale = 0.0f;
    float baseFixedDelta = 0.0f;
    float baseCameraZoom = 0.0f;
    Vector3 baseCameraPos = Vector3.zero;

    CharacterController target;

    private void Start()
    {
        JuiceManager manager = FindObjectOfType<JuiceManager>();
        if (!manager) return;
        baseTimeScale = manager.baseTimeScale;
        baseFixedDelta = manager.baseFixedDelta;
        baseCameraZoom = manager.baseCameraZoom;
        baseCameraPos = manager.baseCameraPos;
    }

    public float GetDistanceToTarget2()
    {
        if (!target) return Mathf.Infinity;
        return (target.transform.position - transform.position).sqrMagnitude;
    }

    public bool IsEffectActive()
    {
        return amount > 0.0f && target;
    }

    // Update is called once per frame
    void Update()
    {
        amount = 0.0f;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, castRadius, transform.up, Mathf.Infinity, characterMask);
        if (hit.rigidbody)
        {
            CharacterController character = hit.rigidbody.GetComponent<CharacterController>();
            if (character && GetComponent<Bullet>().GetOwner() != character)
            {
                float distToChar2 = (character.transform.position - transform.position).sqrMagnitude;
                if (distToChar2 < maxDist * maxDist)
                {
                    target = character;
                    float dist = Mathf.Clamp(Mathf.Sqrt(distToChar2), minDist, maxDist);
                    float distRange = maxDist - minDist;
                    amount = 1.0f - ((dist - minDist) / distRange);
                    //amount = Mathf.Clamp01(amount);
                }
            }
        }
    }

    public void UpdateEffect()
    {
        Bullet bullet = GetComponent<Bullet>();
        Time.timeScale = baseTimeScale * Mathf.Lerp(1.0f, timeMultiplier, amount * bullet.multiplier);
        Time.fixedDeltaTime = baseFixedDelta * Mathf.Lerp(1.0f, timeMultiplier, amount * bullet.multiplier);

        Camera.main.orthographicSize = Mathf.Lerp(baseCameraZoom, maxZoom, amount);
        Vector3 targetPos = target.transform.position;
        targetPos.z = baseCameraPos.z;

        Camera.main.transform.position = Vector3.Lerp(baseCameraPos, targetPos, amount * maxCameraPull);
    }
}
