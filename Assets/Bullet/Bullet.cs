﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class Bullet : MonoBehaviour
{
    public SpriteRenderer sprite;
    public ParticleSystem explosion;
    public TrailRenderer trail;
    public Rigidbody2D body;

    public float speed = 1.0f;
    public float multiplier = 1.0f;

    CharacterController owner;

    bool destroying = false;

    Vector3 direction = Vector3.zero;
    
    public void SetOwner(CharacterController owner)
    {
        this.owner = owner;
    }

    public CharacterController GetOwner()
    {
        return owner;
    }

    void UpdateVelocity()
    {
        body.velocity = direction * speed * multiplier;
    }

    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
        transform.up = this.direction;
        UpdateVelocity();
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
        UpdateVelocity();
    }

    public void SetMultiplier(float multiplier)
    {
        this.multiplier = multiplier;
        UpdateVelocity();
    }

    public void AddMultiplier(float multiplier)
    {
        this.multiplier += multiplier;
        UpdateVelocity();
    }

    private void Update()
    {
        
    }

    IEnumerator WaitForTrailEnd()
    {
        yield return new WaitForSeconds(trail.time);
        yield return new WaitUntil(() => !explosion.isPlaying);
        
        Destroy(gameObject);
    }

    public async void Die()
    {
        destroying = true;
        body.velocity = Vector3.zero;
        explosion.Play();
        Destroy(sprite.gameObject);

        DramaticSlowdown ds = GetComponent<DramaticSlowdown>();
        if (ds) Destroy(ds);

        StartCoroutine(WaitForTrailEnd());
    }
}
