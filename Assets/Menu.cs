﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public bool wipePrefs = false;

    private void Start()
    {
        if (wipePrefs) PlayerPrefs.SetInt("LastLevel", 0);
    }

    public void LoadLevel(int level, bool singlePlayer)
    {
        GameManager.isSinglePlayer = singlePlayer;
        GameManager.LoadLevel(level);
    }

    public void LoadLatestLevel(bool singlePlayer)
    {
        int latest = PlayerPrefs.GetInt("LastLevel", GameManager.levelsOffset);
        int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings - GameManager.levelsOffset - 1;
        if (latest == sceneCount)
        {
            PlayerPrefs.SetInt("LastLevel", GameManager.levelsOffset);
        }
        latest = PlayerPrefs.GetInt("LastLevel", GameManager.levelsOffset);
        LoadLevel(latest, singlePlayer);
    }
}
