﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Vector3 StartPosition;
    private float Duration = 0;
    private float Magnitude = 0;

    private void Awake()
    {
        StartPosition = transform.position;
    }

    private void Update()
    {
        if(Duration > 0)
        {
            transform.position = StartPosition;
            transform.position += Random.insideUnitSphere * Magnitude;
            Duration -= Time.deltaTime;
        }
        else
        {
            transform.position = StartPosition;
        }
    }

    public void ShakeCamera(float _Duration, float _Magnitude)
    {
        Duration = _Duration;
        Magnitude = _Magnitude;
    }
}
