﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour
{
    //SerializeField]
    //Menu canvasMenu;

    bool isSelfCollide = true;

    bool musicIsEnabled = true;

    //public ParticleSystem explosion;

    [SerializeField]
    bool singlePlayerButton = true;

    void OnTriggerEnter2D(Collider2D collision)
    {
            if (musicIsEnabled)
            {
                GetComponent<Text>().color = Color.red;
                SoundManager.instance.StopMusic();
                musicIsEnabled = false;
            }
            else
            {
                GetComponent<Text>().color = Color.green;
                SoundManager.instance.StartMusic();
                musicIsEnabled = true;
            }
    }

}
