﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    [SerializeField]
    Menu canvasMenu;

    bool isSelfCollide = true;

    public ParticleSystem explosion;

    [SerializeField]
    bool singlePlayerButton = true;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isSelfCollide)
        {
            Destroy(GetComponent<UnityEngine.UI.Text>());
            explosion.Play();
            Destroy(GetComponent<Rigidbody2D>());
            StartCoroutine(Play());
        }
        else
        {
            isSelfCollide = false;
        }
    }

    IEnumerator Play()
    {
        yield return new WaitForSeconds(1f);
        canvasMenu.LoadLatestLevel(singlePlayerButton);
    }


}


