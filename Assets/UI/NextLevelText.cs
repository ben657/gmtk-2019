﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class NextLevelText : MonoBehaviour
{
    Player player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach(Player player in ReInput.players.GetPlayers())
        {
            if (player.GetAxis("Select") > 0)
                GameManager.LoadNextLevel();
        }
    }
}
