﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    Image image;
    Text text;

    public float speed = 1.0f;
    public bool startFadedIn = false;

    float a = 0.0f;
    float direction = 1.0f;

    private void Awake()
    {
        image = GetComponent<Image>();
        text = GetComponent<Text>();
        Snap(startFadedIn);
    }

    private void Start()
    {
        
    }

    void UpdateAlpha()
    {
        Color c = image ? image.color : text.color; 
        c.a = a;
        if(image) image.color = c;
        if (text) text.color = c;
    }

    public void Snap(bool faded)
    {
        if (faded)
        {
            a = 1.0f;
            direction = 1.0f;
        }
        else
        {
            a = 0.0f;
            direction = -1.0f;
        }
        UpdateAlpha();
    }

    public void FadeIn(bool snapStart = false)
    {
        if (snapStart) Snap(false);
        direction = 1.0f;
    }

    public void FadeOut(bool snapStart = false)
    {
        if (snapStart) Snap(true);
        direction = -1.0f;
    }

    private void Update()
    {
        a = Mathf.Clamp(a + (direction * Time.deltaTime * speed), 0.0f, 1.0f);
        UpdateAlpha();
    }

    public bool IsFading()
    {
        return (direction > 0.0f && a < 1.0f) || (direction < 0.0f && a > 0.0f);
    }
}
