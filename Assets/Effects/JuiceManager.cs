﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuiceManager : MonoBehaviour
{
    public float baseTimeScale = 0.0f;
    public float baseFixedDelta = 0.0f;
    public float baseCameraZoom = 0.0f;
    public Vector3 baseCameraPos = Vector3.zero;

    private void Start()
    {
        baseTimeScale = Time.timeScale;
        baseFixedDelta = Time.fixedDeltaTime;
        baseCameraZoom = Camera.main.orthographicSize;
        baseCameraPos = Camera.main.transform.position;
    }

    private void Reset()
    {
        Time.timeScale = baseTimeScale;
        Time.fixedDeltaTime = baseFixedDelta;
        if(Camera.main)
        {
            Camera.main.orthographicSize = baseCameraZoom;
            Camera.main.transform.position = baseCameraPos;
        }
    }

    private void OnDestroy()
    {
        print("Ran");
        Reset();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        DramaticSlowdown[] effects = FindObjectsOfType<DramaticSlowdown>();
        if (effects.Length < 1)
        {
            Reset();
            return;
        }

        DramaticSlowdown chosen = effects[0];
        float minDist = chosen.GetDistanceToTarget2();

        foreach(DramaticSlowdown effect in effects)
        {
            if(effect.IsEffectActive() && effect.GetDistanceToTarget2() < minDist)
            {
                chosen = effect;
                minDist = effect.GetDistanceToTarget2();
            }
        }

        if (chosen.IsEffectActive()) chosen.UpdateEffect();
        else Reset();
    }
}
