﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
public struct AIData
{
    public float angle;
    public float time;
}

public class LevelManager : MonoBehaviour
{
    public CharacterController characterPrefab;

    public Transform[] spawnPoints;
    public Transform boundsContainer;
    public Fader fader;
    public Text p1Score;
    public Text p2Score;
    public Color p1Color;
    public Color p2Color;

    public bool portrait = false;

    bool singlePlayer = true;

    public AIData aiData;

    List<CharacterController> characters = new List<CharacterController>();

    public AudioClip PlayerOneWins, PlayerTwoWins;

    bool GameEnded = false;

    void Awake()
    {
        singlePlayer = GameManager.isSinglePlayer;
        CharacterController c1 = Instantiate(characterPrefab);
        c1.gameObject.AddComponent<TouchCharacterController>();
        c1.GetComponentsInChildren<SpriteRenderer>().ToList().ForEach((r) => r.color = p1Color);
        c1.transform.position = spawnPoints[0].position;
        c1.transform.up = Vector3.left;
        c1.showMarker = true;
        characters.Add(c1);

        CharacterController c2 = Instantiate(characterPrefab);
        if (!singlePlayer)
        {
            GamepadCharacterController p2 = c2.gameObject.AddComponent<GamepadCharacterController>();
            p2.playerId = 1;
            c2.showMarker = true;
        }
        else
        {
            c2.gameObject.AddComponent<AICharacterController>();
        }

        c2.GetComponentsInChildren<SpriteRenderer>().ToList().ForEach((r) => r.color = p2Color);
        c2.transform.position = spawnPoints[1].position;
        c2.transform.up = Vector3.right;
        characters.Add(c2);

        GameManager.SetCurrentLevel(SceneManager.GetActiveScene().buildIndex);

        if(Application.platform == RuntimePlatform.Android)
        {
            GameObject container = new GameObject();
            GameObject[] objects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach(GameObject o in objects)
            {
                if (o.transform != transform)
                    o.transform.parent = container.transform;
            }
            boundsContainer.parent = container.transform;
            container.transform.Rotate(Vector3.forward * 90.0f);
            Camera.main.orthographicSize *= 1.777777f;
            aiData.angle -= 90.0f;
        }
    }

    void UpdateScoreText()
    {
        p1Score.text = GameManager.p1Score.ToString();
        p2Score.text = GameManager.p2Score.ToString();
    }

    private void Start()
    {
        fader.FadeOut();
        UpdateScoreText();
    }

    bool GameShouldEnd()
    {
        bool bothFired = true;
        foreach (CharacterController c in characters)
        {
            if (!c) return true;
            if (!c.HasFired()) return false;
        }

        if (!bothFired) return false;

        Bullet[] bullets = FindObjectsOfType<Bullet>();
        return bullets.Length == 0;
    }

    IEnumerator EndGame(bool shouldReload)
    {
        //Player 1 dead
        if (!characters[0] && characters[1])
        {
            SoundManager.instance.PlaySingle(PlayerTwoWins);
            GameManager.p2Score += 1;
        }
        //Player 2 dead
        else if (characters[0] && !characters[1])
        {
            SoundManager.instance.PlaySingle(PlayerOneWins);
            GameManager.p1Score += 1;
        }

        UpdateScoreText();

        Fader p1Fader = p1Score.GetComponent<Fader>();
        p1Fader.FadeIn();
        Fader p2Fader = p2Score.GetComponent<Fader>();
        p2Fader.FadeIn();

        fader.FadeIn(true);

        yield return new WaitWhile(() => p1Fader.IsFading() || p2Fader.IsFading() || fader.IsFading());

        yield return new WaitForSeconds(2);

        p1Fader.FadeOut();
        p2Fader.FadeOut();

        yield return new WaitUntil(() => !p1Fader.IsFading() && !p2Fader.IsFading());

        if (shouldReload) GameManager.Reload();
        else GameManager.LoadNextLevel();
    }

    private void Update()
    {
        if (GameShouldEnd() && !GameEnded)
        {
            GameEnded = true;
            
            bool shouldReload = (characters[0] && characters[1]) || (singlePlayer && !characters[0]);
            StartCoroutine(EndGame(shouldReload));
        }
    }
}
