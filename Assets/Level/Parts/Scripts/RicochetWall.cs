﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RicochetWall : Wall
{
    protected override void OnBulletCollision(Bullet bullet, Collision2D collision)
    {
        Vector3 v = bullet.transform.up;
        Vector3 n = collision.contacts[0].normal;
        bullet.SetPosition(collision.contacts[0].point);
        bullet.SetDirection(Vector3.Reflect(v, n));
    }
}
