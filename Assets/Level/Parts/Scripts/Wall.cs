﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField]
    float speed = 1f;
    [SerializeField]
    float angle = 1f;
    [SerializeField]
    bool isSpinning = false;
    [SerializeField]

    private SpriteRenderer WallVisual;
    private GameObject Copy;
    private float PulseDuration = 0;
    private float spinTime;

    public Color HitColour;
    public float ScreenShakeLength = 0.08f;
    public float ScreenShakeMagnitude = 0.15f;

    public AudioClip OnHitSound;

    private void Awake()
    {
        WallVisual = GetComponentInChildren<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Bullet bullet = collision.gameObject.GetComponent<Bullet>();
        if (bullet == null) return;
        
        OnHit();
        OnBulletCollision(bullet, collision);
    }

    private void OnHit()
    {
        Camera.main.GetComponent<CameraShake>().ShakeCamera(ScreenShakeLength, ScreenShakeMagnitude);

        SoundManager.instance.PlaySingle(OnHitSound);

        PulseDuration = 0.2f;
        Copy = Instantiate<GameObject>(WallVisual.gameObject, WallVisual.transform.position, WallVisual.transform.rotation, WallVisual.transform.parent);
        Destroy(Copy.GetComponent<BoxCollider2D>());
        Copy.GetComponent<SpriteRenderer>().color = HitColour;
        Copy.GetComponent<SpriteRenderer>().sortingOrder = 1;
        Copy.transform.localScale = new Vector3(Copy.transform.localScale.x * 1.05f, Copy.transform.localScale.y * 2, Copy.transform.localScale.z);      
    }
    
    private void Update()
    {
        //transform.Rotate(Vector3.forward * speed * Time.deltaTime);
        if (isSpinning)
        {
            if (angle != 360)
            {
                spinTime = spinTime + Time.deltaTime * speed;
                float phase = Mathf.Sin(spinTime);
                transform.localRotation = Quaternion.Euler(new Vector3(0, 0, phase * angle));
            }
            else
            {
                transform.Rotate(Vector3.forward * speed * Time.deltaTime);
            }

        }

        if (PulseDuration > 0)
        {
            PulseDuration -= Time.deltaTime;
        }
        else
        {
            if (Copy != null)
            {
                Copy.GetComponent<SpriteRenderer>().size = WallVisual.size;
                Destroy(Copy);
                Copy = null;
            }
        }
    }

    protected virtual void OnBulletCollision(Bullet bullet, Collision2D collision)
    {
        bullet.Die();
    }
}
