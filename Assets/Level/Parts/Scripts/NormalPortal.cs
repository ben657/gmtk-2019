﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalPortal : Wall
{
    [SerializeField]
    private Transform partnerPortal;

    [SerializeField]
    private Bullet bulletPrefab;

    protected override void OnBulletCollision(Bullet bullet, Collision2D collision)
    {
        Vector3 v = bullet.transform.up;
        Vector3 n = collision.contacts[0].normal;

        bool inFront = Vector3.Dot(v, transform.up) < 0.0f;
        if (!inFront)
        {
            bullet.SetDirection(Vector3.Reflect(v, n));
        }
        else
        {
            Bullet newBullet = Instantiate(bulletPrefab);
            newBullet.transform.position = partnerPortal.position;
            newBullet.SetDirection(partnerPortal.up);
            newBullet.SetOwner(bullet.GetOwner());

            Physics2D.IgnoreCollision(newBullet.GetComponentInChildren<Collider2D>(), partnerPortal.GetComponentInChildren<Collider2D>());

            bullet.Die();
        }
    }

    private void OnDrawGizmos() 
    {
        Gizmos.DrawRay(transform.position,transform.up);
    }
}
