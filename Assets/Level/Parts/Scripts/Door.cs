﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Mover doorOne;
    public Mover doorTwo;

    public bool isOpened = false;
    // Update is called once per frame

    public float openTime = 3f;
    void Update()
    {
        if (isOpened)
        {
            doorOne.isMoving = true;
            doorTwo.isMoving = true;
        }
    }

    //Used to play single sound clips.
    public void OpenDoor()
    {
        doorOne.invertPoints();
        doorTwo.invertPoints();
        doorOne.isMoving = true;
        doorTwo.isMoving = true;
    }


}
