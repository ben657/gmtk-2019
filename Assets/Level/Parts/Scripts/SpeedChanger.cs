﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedChanger : Wall
{
    [SerializeField]
    private float multiplier = 5f;

    [SerializeField]
    private bool isAdditive = false;

    protected override void OnBulletCollision(Bullet bullet, Collision2D collision)
    {
        if (isAdditive)
        {
            bullet.AddMultiplier(multiplier);
        }
        else
        {
            bullet.SetMultiplier(multiplier);
        }
        
    }
}
