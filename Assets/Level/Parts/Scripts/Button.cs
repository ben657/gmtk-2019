﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Button : RicochetWall
{
    public Door openDoor;
    protected override void OnBulletCollision(Bullet bullet, Collision2D collision)
    {
        base.OnBulletCollision(bullet, collision);

        openDoor.OpenDoor();

    }

}
