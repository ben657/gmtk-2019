﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField]
    Transform startPoint;

    [SerializeField]
    Transform endPoint;

    [SerializeField]
    float speed = 1f;

    Transform destination;

    [SerializeField]
    public bool invertedDoors = false;

    [SerializeField]
    public bool isMoving = true;


    public bool changingDoors = false;

    public void invertPoints()
    {
        Transform tempTransOne = startPoint;
        Transform tempTransTwo = endPoint;

        endPoint = tempTransOne;
        startPoint = tempTransTwo;
        destination = startPoint;
    }

    void Start()
    {
        if (invertedDoors)
        {
            Transform tempTransOne = startPoint;
            Transform tempTransTwo = endPoint;

            endPoint = tempTransOne;
            startPoint = tempTransTwo;
        }
        transform.position = startPoint.position;
        destination = endPoint;
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            float step = speed * Time.deltaTime;
            float tolerance = 0.3f;
            if (changingDoors)
            {
                tolerance = 0.0f;
            }
            if (Vector3.Distance(transform.position, destination.position) < tolerance)
            {
                if (invertedDoors || changingDoors)
                {
                    return;
                }
                if (destination == startPoint)
                {
                    destination = endPoint;
                }
                else
                {
                    destination = startPoint;
                }
            }
            transform.position = Vector2.MoveTowards(transform.position, destination.position, step);
        }
    }



    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(startPoint.position, .5f);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(endPoint.position, .5f);
    }
}
