﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioClip bangingSoundTrack;

    public static SoundManager instance = null;

    private void Start()
    {
        musicSource.clip = bangingSoundTrack;
        musicSource.Play();
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }

    public void StartMusic()
    {
        musicSource.Play();
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    IEnumerator killClip(AudioSource sfx)
    {
        yield return new WaitForSeconds(2f);

        Destroy(sfx);
    }

    //Used to play single sound clips.
    public void PlaySingle(AudioClip clip)
    {
        AudioSource sfx = gameObject.AddComponent<AudioSource>();
        sfx.clip = clip;
        sfx.Play();

        StartCoroutine(killClip(sfx));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
